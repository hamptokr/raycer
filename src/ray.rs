use std::f64::MAX;

use crate::vec3::*;
use crate::geometry::*;
use crate::camera::random_in_unit_disk;

#[derive(Copy, Clone, Debug)]
pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
}

impl Ray {
    pub fn new(origin: Vec3, direction: Vec3) -> Ray {
        Ray {
            origin,
            direction,
        }
    }

    pub fn point_at_parameter(&self, t: f64) -> Vec3 {
        self.origin + t * self.direction
    }
}

pub fn hit_sphere(center: &Vec3, radius: f64, r: &Ray) -> f64 {
    let oc: Vec3 = r.origin - *center;
    let a = r.direction.dot_product(r.direction);
    let b = 2.0 * oc.dot_product(r.direction);
    let c = oc.dot_product(oc) - radius * radius;
    let discriminant = b * b - 4.0 * a * c;
    if discriminant < 0.0 {
        -1.0
    } else {
        (-b - discriminant.sqrt()) / (2.0 * a)
    }
}

pub fn color_ray(r: &Ray, w: &Vec<Box<Hitable>>, depth: i32) -> Vec3 {
    let hit: Option<HitRecord> = w.hit(r, 0.001, MAX);

    match hit {
        Some(hr) => {
            let mut scattered = Ray::new(Vec3::zeroed(), Vec3::zeroed());
            let mut attenuation = Vec3::zeroed();
            if depth < 50 && hr.material.scatter(r, &hr, &mut attenuation, &mut scattered) {
                attenuation * color_ray(&scattered, w, depth + 1)
            } else {
                Vec3::zeroed()
            }
        },
        None => {
            let unit_direction = r.direction.unit_vector();
            let t = 0.5 * unit_direction.y() + 1.0;
            (1.0 - t) * Vec3::new(1.0, 1.0, 1.0) + t * Vec3::new(0.5, 0.7, 1.0)
        }
    }
}
