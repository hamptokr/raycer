use crate::vec3::Vec3;
use crate::material::Material;
use crate::ray::Ray;
use crate::geometry::HitRecord;
use crate::camera::random_in_unit_disk;

pub struct Metal {
    albedo: Vec3,
    fuzz: f64,
}

impl Metal {
    pub fn new(a: Vec3, f: f64) -> Metal {
        let mut fuzz_factor: f64 = 0.0;
        if f < 1.0 {
            fuzz_factor = f;
        } else {
            fuzz_factor = 1.0;
        }
        Metal {
            albedo: a,
            fuzz: fuzz_factor,
        }
    }
}

impl Material for Metal {
    fn scatter(&self, r_in: &Ray, rec: &HitRecord, attenuation: &mut Vec3, scattered: &mut Ray) -> bool {
        let reflected = r_in.direction.unit_vector().reflect(&rec.normal);
        *scattered = Ray::new(rec.p, reflected + self.fuzz * random_in_unit_disk());
        *attenuation = self.albedo.clone();
        scattered.direction.dot_product(rec.normal) > 0.0
    }
}
