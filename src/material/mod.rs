use crate::ray::Ray;
use crate::geometry::HitRecord;
use crate::vec3::Vec3;

pub mod lambertian;
pub mod metal;
pub mod dielectric;

pub trait Material {
    fn scatter(&self, r_in: &Ray, rec: &HitRecord, attenuation: &mut Vec3, scattered: &mut Ray) -> bool;
}
