use crate::material::Material;
use crate::ray::Ray;
use crate::geometry::HitRecord;
use crate::vec3::Vec3;
use rand::Rng;

#[derive(Copy, Clone)]
pub struct Dielectric {
    ref_idx: f64,
}

impl Dielectric {
    pub fn new(ri: f64) -> Dielectric {
        Dielectric {
            ref_idx: ri,
        }
    }

    pub fn schlick(&self, cosine: f64) -> f64 {
        let mut r0 = (1.0 - self.ref_idx) / (1.0 + self.ref_idx);
        r0 = r0 * r0;
        r0 + (1.0 - r0) * (1.0 - cosine).powi(5)
    }
}

impl Material for Dielectric {
    fn scatter(&self, r_in: &Ray, rec: &HitRecord, attenuation: &mut Vec3, scattered: &mut Ray) -> bool {
        let mut outward_normal = Vec3::zeroed();
        let reflected = r_in.direction.reflect(&rec.normal);
        let mut ni_over_nt = 0.0;
        *attenuation = Vec3::new(1.0, 1.0, 1.0);
        let mut refracted = Vec3::zeroed();
        let mut reflect_prob = 0.0;
        let mut cosine = 0.0;
        if r_in.direction.dot_product(rec.normal) > 0.0 {
            outward_normal = -rec.normal;
            ni_over_nt = self.ref_idx;
            cosine = self.ref_idx * r_in.direction.dot_product(rec.normal) / r_in.direction.length();
        } else {
            outward_normal = rec.normal;
            ni_over_nt = 1.0 / self.ref_idx;
            cosine = -(r_in.direction.dot_product(rec.normal)) / r_in.direction.length();
        }

        if r_in.direction.refract(&outward_normal, ni_over_nt, &mut refracted) {
            reflect_prob = self.schlick(cosine);
        } else {
            reflect_prob = 1.0;
        }

        if rand::random::<f64>() < reflect_prob {
            *scattered = Ray::new(rec.p, reflected);
        } else {
            *scattered = Ray::new(rec.p, refracted);
        }

        true
    }
}
