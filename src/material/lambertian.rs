use crate::vec3::Vec3;
use crate::material::Material;
use crate::ray::Ray;
use crate::geometry::HitRecord;
use crate::camera::random_in_unit_disk;

#[derive(Copy, Clone, Debug)]
pub struct Lambertian {
    pub albedo: Vec3,
}

impl Lambertian {
    pub fn new(a: Vec3) -> Lambertian {
        Lambertian {
            albedo: a,
        }
    }
}

impl Material for Lambertian {
    fn scatter(&self, r_in: &Ray, rec: &HitRecord, attenuation: &mut Vec3, scattered: &mut Ray) -> bool {
        let target = rec.p + rec.normal + random_in_unit_disk();
        *scattered = Ray::new(rec.p, target - rec.p);
        *attenuation = self.albedo.clone();
        true
    }
}
