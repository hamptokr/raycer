use crate::vec3::*;
use crate::ray::Ray;
use rand::Rng;
use std::f64::consts::PI;

#[derive(Copy, Clone, Debug)]
pub struct Camera {
    pub origin: Vec3,
    pub horizontal: Vec3,
    pub vertical: Vec3,
    pub lower_left_corner: Vec3,
    pub u: Vec3,
    pub v: Vec3,
    pub w: Vec3,
    pub lens_radius: f64,
}

impl Camera {
    pub fn new(look_from: Vec3, look_at: Vec3, v_up: Vec3,v_fov: f64, aspect: f64, aperture: f64, focus_dist: f64) -> Camera {
        let lens_radius = aperture / 2.0;
        let mut u = Vec3::zeroed();
        let mut v = Vec3::zeroed();
        let mut w = Vec3::zeroed();
        let mut origin = look_from.clone();

        let theta = v_fov * PI / 180.0;
        let half_height = (theta / 2.0).tan();
        let half_width = aspect * half_height;

        w = (look_from - look_at).unit_vector();
        u = v_up.cross_product(w).unit_vector();
        v = w.cross_product(u);

        let mut lower_left_corner = Vec3::new(-half_width, -half_height, -1.0);
        lower_left_corner = origin - half_width * focus_dist * u - half_height * focus_dist * v - w * focus_dist;

        Camera {
            lower_left_corner,
            horizontal: 2.0 * half_width * focus_dist * u,
            vertical: 2.0 * half_height * focus_dist * v,
            origin,
            u,
            v,
            w,
            lens_radius,
        }
    }

    pub fn get_ray(&self, s: f64, t: f64) -> Ray {
        let rd = self.lens_radius * random_in_unit_disk();
        let offset = self.u * rd.x() + self.v * rd.y();
        Ray::new(self.origin + offset, self.lower_left_corner + s * self.horizontal + t * self.vertical - self.origin - offset)
    }
}

pub fn random_in_unit_disk() -> Vec3 {
    let mut rng = rand::thread_rng();
    loop {
        let p = 2.0 * Vec3::new(rng.gen::<f64>(), rng.gen::<f64>(), 0.0) - Vec3::new(1.0, 1.0, 0.0);
        if p.dot_product(p) < 1.0 {
            return p;
        }
    }
}