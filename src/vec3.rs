use std::ops;

#[derive(Copy, Clone, Debug)]
pub struct Vec3 {
    elements: [f64; 3],
}

impl Vec3 {
    pub fn new(e0: f64, e1: f64, e2: f64) -> Vec3 {
        Vec3 {
            elements: [e0, e1, e2],
        }
    }

    pub fn zeroed() -> Vec3 {
        Vec3 {
            elements: [0.0, 0.0, 0.0]
        }
    }

    pub fn cross_product(&self, v2: Vec3) -> Vec3 {
        Vec3 {
            elements: [
                self[1] * v2[2] - self[2] * v2[1],
                -(self[0] * v2[2] - self[2] * v2[0]),
                self[0] * v2[1] - self[1] * v2[0]
            ]
        }
    }

    pub fn unit_vector(&self) -> Vec3 {
        *self / self.length()
    }

    pub fn x(&self) -> f64 {
        self.elements[0]
    }

    pub fn y(&self) -> f64 {
        self.elements[1]
    }

    pub fn z(&self) -> f64 {
        self.elements[2]
    }

    pub fn r(&self) -> f64 {
        self.elements[0]
    }

    pub fn g(&self) -> f64 {
        self.elements[1]
    }

    pub fn b(&self) -> f64 {
        self.elements[2]
    }

    pub fn length(&self) -> f64 {
        (self[0] * self[0] + self[1] * self[1] + self[2] * self[2]).sqrt()
    }

    pub fn squared_length(&self) -> f64 {
        self[0] * self[0] + self[1] * self[1] + self[2] * self[2]
    }

    pub fn make_unit_vector(&mut self) {
        let k: f64 = 1.0 / self.length();
        self[0] *= k;
        self[1] *= k;
        self[2] *= k;
    }

    pub fn dot_product(&self, v2: Vec3) -> f64 {
        self[0] * v2[0] + self[1] * v2[1] + self[2] * v2[2]
    }

    pub fn reflect(&self, n: &Vec3) -> Vec3{
        *self - 2.0 * self.dot_product(*n) * *n
    }

    pub fn refract(&self, n: &Vec3, ni_over_nt: f64, refracted: &mut Vec3) -> bool {
        let uv = self.unit_vector();
        let dt = uv.dot_product(*n);
        let discriminant = 1.0 - ni_over_nt * ni_over_nt * (1.0-dt * dt);
        if discriminant > 0.0 {
            *refracted = ni_over_nt * (uv - *n * dt) - *n * discriminant.sqrt();
            true
        } else {
            false
        }
    }
}

impl ops::Neg for Vec3 {
    type Output = Vec3;

    fn neg(self) -> Self::Output {
        Vec3 {
            elements: [
                -self.elements[0],
                -self.elements[1],
                -self.elements[2],
            ]
        }
    }
}

impl ops::Add for Vec3 {
    type Output = Vec3;

    fn add(self, other: Vec3) -> Vec3 {
        Vec3 {
            elements: [
                self.elements[0] + other.elements[0],
                self.elements[1] + other.elements[1],
                self.elements[2] + other.elements[2]
            ]
        }
    }
}

impl ops::AddAssign for Vec3 {
    fn add_assign(&mut self, other: Vec3) {
        *self = Vec3 {
            elements: [
                self.elements[0] + other.elements[0],
                self.elements[1] + other.elements[1],
                self.elements[2] + other.elements[2]
            ]
        }
    }
}

impl ops::Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, other: Vec3) -> Vec3 {
        Vec3 {
            elements: [
                self.elements[0] - other.elements[0],
                self.elements[1] - other.elements[1],
                self.elements[2] - other.elements[2]
            ]
        }
    }
}

impl ops::SubAssign for Vec3 {
    fn sub_assign(&mut self, other: Vec3) {
        *self = Vec3 {
            elements: [
                self.elements[0] - other.elements[0],
                self.elements[1] - other.elements[1],
                self.elements[2] - other.elements[2]
            ]
        }
    }
}

impl ops::Mul for Vec3 {
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Vec3 {
        Vec3 {
            elements: [
                self.elements[0] * other.elements[0],
                self.elements[1] * other.elements[1],
                self.elements[2] * other.elements[2]
            ]
        }
    }
}

impl ops::MulAssign for Vec3 {
    fn mul_assign(&mut self, other: Vec3) {
        *self = Vec3 {
            elements: [
                self.elements[0] * other.elements[0],
                self.elements[1] * other.elements[1],
                self.elements[2] * other.elements[2]
            ]
        }
    }
}

impl ops::Div for Vec3 {
    type Output = Vec3;

    fn div(self, other: Vec3) -> Vec3 {
        Vec3 {
            elements: [
                self.elements[0] / other.elements[0],
                self.elements[1] / other.elements[1],
                self.elements[2] / other.elements[2]
            ]
        }
    }
}

impl ops::DivAssign for Vec3 {
    fn div_assign(&mut self, other: Vec3) {
        *self = Vec3 {
            elements: [
                self.elements[0] / other.elements[0],
                self.elements[1] / other.elements[1],
                self.elements[2] / other.elements[2]
            ]
        }
    }
}

impl ops::Index<usize> for Vec3 {
    type Output = f64;

    fn index(&self, e: usize) -> &f64 {
        &self.elements[e]
    }
}

impl ops::IndexMut<usize> for Vec3 {
    fn index_mut(&mut self, e: usize) -> &mut f64 {
        &mut self.elements[e]
    }
}

impl ops::Mul<f64> for Vec3 {
    type Output = Vec3;

    fn mul(self, c: f64) -> Vec3 {
        Vec3 {
            elements: [
                self[0] * c,
                self[1] * c,
                self[2] * c
            ]
        }
    }
}

impl ops::Div<f64> for Vec3 {
    type Output = Vec3;

    fn div(self, c: f64) -> Vec3 {
        Vec3 {
            elements: [
                self[0] / c,
                self[1] / c,
                self[2] / c
            ]
        }
    }
}

impl ops::DivAssign<f64> for Vec3 {
    fn div_assign(&mut self, rhs: f64) {
        *self = Vec3 {
            elements: [
                self[0] / rhs,
                self[1] / rhs,
                self[2] / rhs
            ]
        }
    }
}

impl ops::Mul<Vec3> for f64 {
    type Output = Vec3;

    fn mul(self, v: Vec3) -> Vec3 {
        Vec3 {
            elements: [
                v[0] * self,
                v[1] * self,
                v[2] * self
            ]
        }
    }
}

impl ops::Div<Vec3> for f64 {
    type Output = Vec3;

    fn div(self, v: Vec3) -> Vec3 {
        Vec3 {
            elements: [
                v[0] / self,
                v[1] / self,
                v[2] / self
            ]
        }
    }
}
