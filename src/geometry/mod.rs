use crate::vec3::*;
use crate::ray::Ray;
use crate::material::Material;

#[derive(Copy, Clone)]
pub struct HitRecord<'a> {
    pub t: f64,
    pub p: Vec3,
    pub normal: Vec3,
    pub material: &'a Material,
}

pub trait Hitable {
    fn hit(&self, r: &Ray, tmin: f64, tmax: f64) -> Option<HitRecord>;
}

impl Hitable for Vec<Box<Hitable>> {
    fn hit(&self, r: &Ray, tmin: f64, tmax: f64) -> Option<HitRecord> {
        let mut hit: Option<HitRecord> = None;

        for hitable in self.iter() {
            if let Some(possible_hit) = hitable.hit(r, tmin, tmax) {
                match hit {
                    None => hit = Some(possible_hit),
                    Some(prev_hit) => if possible_hit.t < prev_hit.t {
                        hit = Some(possible_hit);
                    }
                }
            }
        }

        hit
    }
}

pub struct Sphere {
    center: Vec3,
    radius: f64,
    material: Box<Material>,
}

impl Sphere {
    pub fn new(c: Vec3, r: f64, m: Box<Material>) -> Sphere {
        Sphere {
            center: c,
            radius: r,
            material: m,
        }
    }
}

impl Hitable for Sphere {
    fn hit(&self, r: &Ray, tmin: f64, tmax: f64) -> Option<HitRecord> {
        let oc = r.origin - self.center;
        let a = r.direction.dot_product(r.direction);
        let b = oc.dot_product(r.direction);
        let c = oc.dot_product(oc) - self.radius * self.radius;
        let discriminant = b * b - a * c;

        if discriminant > 0.0 {
            let mut t = (-b - discriminant.sqrt()) / a;
            if t < tmax && t > tmin {
                let p = r.point_at_parameter(t);
                return Some(HitRecord {
                    t,
                    p,
                    normal: (p - self.center) / self.radius,
                    material: &*self.material
                })
            }
            t = (-b + discriminant.sqrt()) / a;
            if t < tmax && t > tmin {
                let p = r.point_at_parameter(t);

                return Some(HitRecord {
                    t,
                    p,
                    normal: (p - self.center) / self.radius,
                    material: &*self.material
                })
            }
        }

        None
    }
}
