use std::fs::File;
use std::io::prelude::*;
use crate::vec3::*;
use crate::ray::*;
use crate::geometry::{Sphere, Hitable};
use crate::camera::Camera;
use rand::{Rng, thread_rng};
use crate::material::lambertian::Lambertian;
use crate::material::metal::Metal;
use crate::material::dielectric::Dielectric;

mod vec3;
mod ray;
mod geometry;
mod camera;
mod material;

const PPM_VERSION: &str = "P3";
const MAX_COLOR_VALUE: u8 = 255;

fn main() -> Result<(), Box<std::error::Error>> {
    let nx = 1920;
    let ny = 1080;
    let ns = 25;
    let mut out_image = File::create("out.ppm")?;
    write!(out_image, "{}\n{} {}\n{}\n", PPM_VERSION, nx, ny, MAX_COLOR_VALUE)?;

    let world: Vec<Box<Hitable>> = render_random_scene();

    let look_from = Vec3::new(10.0, 1.8, 2.4);
    let look_at = Vec3::new(0.0, 0.0, 0.5);
    let dist_to_focus = (look_from - Vec3::new(4.0, 1.0, 0.0)).length();
    let aperture = 0.1;

    let camera = Camera::new(
        look_from,
        look_at,
        Vec3::new(0.0, 1.0, 0.0),
        30.0,
        nx as f64 / ny as f64,
        aperture,
        dist_to_focus);

    let mut rng = rand::thread_rng();

    for j in (0..=ny-1).rev() {
        for i in 0..nx {
            let mut col = Vec3::new(0.0, 0.0, 0.0);
            for s in 0..ns {
                let u = (i as f64 + rng.gen::<f64>()) / nx as f64;
                let v = (j as f64 + rng.gen::<f64>()) / ny as f64;
                let r = camera.get_ray(u, v);
                let p = r.point_at_parameter(2.0);
                col += color_ray(&r, &world, 0);
            }
            col /= ns as f64;
            let new_col = Vec3::new(col[0].clone().sqrt(), col[1].clone().sqrt(), col[2].clone().sqrt());
            let ir = (255.99 * new_col[0]) as i32;
            let ig = (255.99 * new_col[1]) as i32;
            let ib = (255.99 * new_col[2]) as i32;

            write!(out_image, "{} {} {}\n", ir, ig, ib)?;
        }
    }

    Ok(())
}

fn render_random_scene() -> Vec<Box<Hitable>> {
    let n = 500;
    let mut list: Vec<Box<Hitable>> = Vec::with_capacity(n + 1);
    list.push(Box::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        Box::new(Lambertian::new(Vec3::new(0.5, 0.5, 0.5)))
    )));

    let mut rng = thread_rng();

    let mut i = 1;
    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = rng.gen::<f64>();
            let center = Vec3::new(a as f64 + 0.9 * rng.gen::<f64>(), 0.2, b as f64 + 0.9 * rng.gen::<f64>());
            if (center - Vec3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                if choose_mat < 0.8 { // diffuse
                    list.push(Box::new(Sphere::new(
                        center,
                        0.2,
                        Box::new(Lambertian::new(Vec3::new(rng.gen::<f64>() * rng.gen::<f64>(), rng.gen::<f64>() * rng.gen::<f64>(), rng.gen::<f64>() * rng.gen::<f64>())))
                    )));
                    i += 1;
                } else if choose_mat < 0.95 { // metal
                    list.push(Box::new(Sphere::new(
                        center,
                        0.2,
                        Box::new(Metal::new(Vec3::new(0.5 * (1.0 + rng.gen::<f64>()), 0.5 * (1.0 + rng.gen::<f64>()), 0.5 * rng.gen::<f64>()), 0.0))
                    )));
                    i += 1;
                } else { // glass
                    list.push(Box::new(Sphere::new(
                        center,
                        0.2,
                        Box::new(Dielectric::new(1.5))
                    )));
                    i += 1;
                }
            }
        }
    }

    list.push(Box::new(Sphere::new(
        Vec3::new(0.0, 1.0, 0.0),
        1.0,
        Box::new(Dielectric::new(1.5))
    )));
    i += 1;

    list.push(Box::new(Sphere::new(
        Vec3::new(4.0, 1.0, 0.0),
        1.0,
        Box::new(Lambertian::new(Vec3::new(0.4, 0.2, 0.1)))
    )));
    i += 1;

    list.push(Box::new(Sphere::new(
        Vec3::new(-4.0, 1.0, 0.0),
        1.0,
        Box::new(Metal::new(Vec3::new(0.7, 0.6, 0.5), 0.0))
    )));
    i += 1;


    list
}
